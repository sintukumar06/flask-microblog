import os


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    # SQLALCHEMY_DATABASE_URI = 'mysql://python:python@localhost:3306/python'
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://python:python@localhost:3306/python'
    # SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'mysql://python:python@localhost:3306/python'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    POSTS_PER_PAGE = 3
    # -----------------ELASTIC SEARCH SETUP-----------------------------------
    ELASTICSEARCH_URL = 'http://localhost:9200'
    # -----------------EMAIL SETUP--------------------------------------------
    MAIL_SERVER = 'localhost'
    MAIL_PORT = 8025
    MAIL_USE_TLS = False
    MAIL_USERNAME = ''
    MAIL_PASSWORD = ''
    ADMINS = ['sintu.development@gmail.com']
    # ------------------------------------------------------------------------
